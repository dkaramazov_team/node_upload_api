# Node Upload API #

### Purpose ###

* Node uploads with multer and grid file system
* Version 1.0

### Setup ###

* Setup mongo database and provide correct URI
* Put mongo database username and password into dot env file with DB_USERNAME and DB_PASSWORD
* Run npm i
* Run npm run start
* To deploy follow instructions in Dockerfile

### Technologies ###

* Nodemon
* dotenv
* Docker
* Bootstrap 4
