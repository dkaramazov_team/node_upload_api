const path = require('path');
const crypto = require('crypto');
const mongoose = require('mongoose');
const multer = require('multer');
const GridFsStorage = require('multer-gridfs-storage');
const Grid = require('gridfs-stream');

const mongoURI = `mongodb://${process.env.DB_USERNAME}:${process.env.DB_PASSWORD}@ds257858.mlab.com:57858/node_uploads`;
const conn = mongoose.createConnection(mongoURI);

let gfs;
conn.once('open', () => {
    gfs = Grid(conn.db, mongoose.mongo);
    gfs.collection('uploads');
});

const storage = new GridFsStorage({
    url: mongoURI,
    file: (req, file) => {
        return new Promise((resolve, reject) => {
            crypto.randomBytes(16, (err, buf) => {
                if (err) {
                    return reject(err);
                }
                const filename = buf.toString('hex') + path.extname(file.originalname);
                const fileInfo = {
                    filename: filename,
                    bucketName: 'uploads'
                };
                resolve(fileInfo);
            });
        });
    }
});
const upload = multer({ storage });

exports.uploadConfig = upload;

exports.getFiles = (cb) => {
    gfs.files.find().toArray((err, files) => {
        if (!files || files.length === 0) {
            return cb(false);
        }
        return cb(files);
    });
};

exports.getFileByName = (name, cb) => {
    gfs.files.findOne({ filename: name }, (err, file) => {
        if (!file || file.length === 0) {
            return cb(false);
        }
        return cb(file);
    });
};

exports.remove = (fileId, cb) => {
    gfs.remove({ _id: fileId, root: 'uploads' }, (err, gridStore) => {
        return err ? cb(err):cb(null);
    });
};

exports.getImage = (name, cb) => {
    gfs.files.findOne({ filename: name }, (err, file) => {
        if (!file || file.length === 0) {
            return cb({err: 'No file exists'}, null);            
        }
        if (file.contentType === 'image/jpg' || file.contentType === 'image/png') {
            return cb(false, (target) => {
                const readStream = gfs.createReadStream(file.filename);
                readStream.pipe(target);
            });
        } else {
            return cb({err: 'not an image'}, null);
        }
    });
};

exports.loadHome = (cb) => {
    gfs.files.find().toArray((err, files) => {
        if (!files || files.length === 0) {
            cb(false);
        } else {
            files.map(file => {
                if (
                    file.contentType === 'image/jpeg' ||
                    file.contentType === 'image/png'
                ) {
                    file.isImage = true;
                } else {
                    file.isImage = false;
                }
            });
            cb(files);
        }
    });
};