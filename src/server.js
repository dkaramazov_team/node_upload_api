const express = require('express');
const bodyParser = require('body-parser');
const methodOverride = require('method-override');
const path = require('path');
const app = express();

app.set('view engine', 'ejs');
app.set('views', path.join(__dirname, 'views'));

app.use(bodyParser.json());
app.use(methodOverride('_method'));

const upload = require('./routes/upload');
const home = require('./routes/home');

app.use('/', home);
app.use('/upload', upload);

module.exports = app;