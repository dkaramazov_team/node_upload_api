const path = require('path');
const crypto = require('crypto');
const mongoose = require('mongoose');
const multer = require('multer');
const GridFsStorage = require('multer-gridfs-storage');
const Grid = require('gridfs-stream');

const mongoURI = `mongodb://${process.env.DB_USERNAME}:${process.env.DB_PASSWORD}@ds257858.mlab.com:57858/node_uploads`;
const conn = mongoose.createConnection(mongoURI);

const express = require('express');
const router = express.Router();
const app = require('../server');

const fileController = require('../controllers/fileController');

router.post('/', fileController.uploadConfig.single('file'), (req, res) => {
    res.redirect('/');
});

router.get('/files', (req, res) => {
    fileController.getFiles((files) => {
        if (files) {
            return res.json(files);
        } else {
            return res.status(404).json({
                err: 'No files exist'
            });
        }
    });
});

router.get('/files/:filename', (req, res) => {
    fileController.getFileByName(req.params.filename, (file) => {
        if (file) {
            return res.json(file);
        } else {
            return res.status(404).json({
                err: 'No file exists'
            });
        }
    });
});

router.delete('/files/:id', (req, res) => {
    fileController.remove(req.params.id, (err) => {
        if (err) {
            return res.status(404).json({ err: err });
        } else {
            res.redirect('/');
        }
    })
});

router.get('/image/:filename', (req, res) => {
    fileController.getImage(req.params.filename, (err, cb) => {
        if (err) {
            return res.status(404).json();
        } else {
            cb(res);
        }
    });
});

module.exports = router;