const express = require('express');
const router = express.Router();
const app = require('../server');

const fileController = require('../controllers/fileController');

router.get('/', (req, res) => {
    fileController.loadHome((files) => {
        if(files){
            res.render('index', { files: files });
        } else {
            res.render('index', { files: false });
        }
    });    
});

module.exports = router;